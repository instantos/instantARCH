<div align="center">
    <h1>instantARCH</h1>
    <p>the official instantOS installer</p>
    <img width="300" height="300" src="https://github.com/instantOS/instantLOGO/blob/master/png/arch.png">
</div>

----------

instantARCH is a super lightweight and quick & easy to use installer for instantOS.

<p align="center">
  <img src="https://github.com/instantOS/instantLOGO/blob/master/screeenshots/instantarch.png">
</p>
